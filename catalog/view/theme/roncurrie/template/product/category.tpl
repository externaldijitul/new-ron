<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <h2 class="cat-heading"><?php echo $heading_title; ?></h2>
  <div class="pagination">
     <div class="col-sm-6 results"><?php echo $results; ?></div>
   <div class="col-sm-6 pagin"><?php echo $pagination; ?></div>
  </div><!-- pagination -->
  <hr>
  <div class="row">
  
  <div class="column-left-main col-sm-3">
    <div class="text-right">

      <div class="panel-heading">Filter options</div>
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="text-right">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="text-right cat-limit">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <?php echo $column_left; ?>
      
  </div><!-- column-left-main -->
  
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      
      <?php if ($categories) { ?>

      <div class="row">

          <ul class="list-group">
            <?php foreach ($categories as $category) { ?>
            <div class="col-grid"><a href="<?php echo $category['href']; ?>">
            <?php if($category['image']) { ?>
              <img src="image/<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" width="201" style="height: 201px; overflow: hidden;" class="product-preview">
            <?php } else { ?>
              <img src="http://headphonespares.sennheiser.co.uk/gfx/default-missing-category-placeholder.png" alt="">
            <?php } ?>
            <div class="sub-cat-block"><?php echo $category['name']; ?></div></a></div>
            <?php } ?>
          </ul>
        </div>

      <?php } ?>
      <?php if ($products) { ?>

        
      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-grid col-sm-3">
          <div class="product-thumb">
            <div class="image"><a href="<?php echo $product['href']; ?>">
            <?php if($product['thumb']) { ?>
            <img src="image/<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
            <?php } else { ?>
            <img src="http://headphonespares.sennheiser.co.uk/gfx/default-missing-category-placeholder.png" alt="">
            <?php } ?>
            </a></div>
            <div>
              <div class="caption">
                <h4><a href="<?php echo $product['href']; ?>">
                <?php
                if ($product['shorttitle']) {
                  echo $product['shorttitle'];
                } else { 
                  echo substr($product['name'], 0, 32) .'...';
                } ?>
                </a></h4>
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  From: <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span>
                  <?php } ?>
                </p>

                <small class="quantity">Quantity: <?php echo $product['quantity']; ?></small>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">

        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        
      </div>
            </div>
      <br />
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
