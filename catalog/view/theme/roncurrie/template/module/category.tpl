<div class="list-group">
  <?php foreach ($categories as $category) { ?>
  <?php if ($category['category_id'] == $category_id) { ?>

  <a href="<?php echo $category['href']; ?>" class="list-group-item active"><?php echo $category['name']; ?></a>
  <?php if ($category['children']) { ?>
  <?php foreach ($category['children'] as $child) { ?>
  <?php if ($child['category_id'] == $child_id) { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } else { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
   <div class="col-grid">
  <div class="product-preview">
  <a href="<?php echo $category['href']; ?>" class="blend-background">
  <?php if ($category['image']) { ?>
  <img src="image/<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>">
  <?php } else { ?>
  <img src="http://headphonespares.sennheiser.co.uk/gfx/default-missing-category-placeholder.png" alt="<?php echo $category['name']; ?>">
  <?php } ?>
  </a>
  </div><!-- product-preview -->
  <a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['name']; ?></a>
  </div><!-- col-grid -->
  <?php } ?>
  <?php } ?>
</div>
