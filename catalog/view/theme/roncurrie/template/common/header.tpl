<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/roncurrie/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/roncurrie/stylesheet/custom.css" rel="stylesheet">
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
<script type="text/javascript">
$(document).ready(function() {
//overlay for the menu hover over 

      $('.navbar .nav li').mouseover(function() {
        $('.overlay').fadeIn();
      });
      $('.navbar .nav').mouseleave(function(){
        $('.overlay').fadeOut();
      });
      $('.guideline').click(function(){
      $('.guidelinestock').addClass('active');
      $('.overlayguideline').fadeIn();
      });
      $('.guideclose').click(function(){
      $('.overlayguideline').fadeOut();
      $('.guidelinestock.active').removeClass('active');
      });
});

</script>
</head>
<body class="<?php echo $class; ?>">
<div class="overlay"></div>
<div class="overlayguideline"></div>
<nav id="top">
<div class="top-inner">
  <div class="container">

  <div class="mini-nav">
    <ul>
    <li><a href="<?php echo $home; ?>"><i class="glyphicon glyphicon-home"></i></i><a href="index.php?route=information/information&information_id=4"> About</a> | </li>
    <li><a href="index.php?route=information/contact">Contact</a> | </li>
    <li><a href="index.php?route=information/information&information_id=6">Delivery</a> | </li>
    </ul>
  </div><!-- mini-nav -->

        <div class="contact-center">
        <ul>
        <li><a href="tel:<?php echo $telephone; ?>"><span ><img src="catalog/view/theme/roncurrie/image/phone-icon.png" alt=""></span><span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></a></li>
        <li><a href="mailto:sales@roncurrie.co.uk?Subject=Your%20Message"><span><img src="catalog/view/theme/roncurrie/image/mail-icon.png" style="width:23px;" alt=""></span><span class="hidden-xs hidden-sm hidden-md"><?php echo $email;?></span></a></li>
        </ul>
        </div><!-- contact-center -->

   <div class="account-module inline-top">
              <ul>
              <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a><span> | </span></li>
              <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
              </ul>
            </div><!-- account-module -->
   </div><!-- contact-center -->

    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
  
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <?php } ?>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
<?php if ($categories) { ?>

<nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div><!-- navbar-header -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <?php if (isset($child['children_3']) && $child['children_3']) { ?>
                    <li class="has-menu"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                    <ul class="sub-menu-in-block">
                    <?php foreach (array_chunk($child['children_3'], ceil(count($child['children_3']) / $child['column'])) as $children_3) { ?>
                           
                              <?php foreach ($children_3 as $child_3) { ?>
                              
                                <li><a href="<?php echo $child_3['href']; ?>"><?php echo $child_3['name']; ?></a></li>
                              
                              <?php } ?>
                          
                              <?php } ?>
                    </ul><!-- sub-menu-in-block -->
                    </li>
                    <?php } else { ?>
                      <li class="has-menu"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                    <?php } ?>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>

<div class="container">
</div>
<?php } ?>
