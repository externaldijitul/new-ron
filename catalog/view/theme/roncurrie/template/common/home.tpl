<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
    <div class="contact-banner">
        <div class="container">
            <div class="col-md-6 inline-content">
                <h3 style="margin-bottom: 20px;"><highlight-span>Ron Currie & Sons - Specialist Timber Merchants</highlight-span></h3>
                <p>We are a family run timber merchant established in 1974. Based in Sutton-in-Ashfield, we have supplied timber and joinery products to Sutton-in-Ashfield, Mansfield, Kirkby-in-Ashfield, Mansfield Woodhouse, Annesley, Clipstone, Alfreton and other areas surrounding Sutton-in-Ashfield for many years. We now offer this same service nationally using courier networks. We are a diverse company offering many different products and ranges in addition to our core timber supply business.</p>
            </div><!-- col-md-6 -->
            <div class="col-md-4 top-margin">
        <form id="contact_form" action="#" method="POST" enctype="multipart/form-data">
        <div class="row">
            <h3><highlight-span>Get in Touch today</highlight-span></h3>
            <label for="name">Your name:</label><br />
            <input id="name" class="input" name="name" type="text" value="" size="30" placeholder="Name"/><br />
        </div>
        <div class="row">
            <label for="email">Your email:</label><br />
            <input id="email" class="input" name="email" type="text" value="" size="30" placeholder="Email"/><br />
        </div>
            <div class="row">
            <label for="email">Your email:</label><br />
            <input id="email" class="input" name="email" type="text" value="" size="30" placeholder="Phone"/><br />
        </div>
        <div class="row">
        <div class="textarea-pull">
            <label for="message">Your message:</label><br />
            <textarea id="message" class="input" name="message" rows="7" cols="30" placeholder="Your Message"></textarea><br />
        <input id="submit_button" type="submit" value="SEND" />
        </div><!-- textarea-pull -->
    </form>                     
    </div>
            </div><!-- col-md-6 -->
        </div><!-- container -->
    </div><!-- contact-banner -->

<?php echo $footer; ?>