</div><!-- close out wrapper -->

<hr class="footer-line">
<div>
<footer>
  <div class="footer-container">
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-4 responsive-callback">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled side-by-side">
          <?php foreach ($informations as $information) { ?>
          <li><i class="glyphicon glyphicon-menu-right"></i><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
          <li><i class="glyphicon glyphicon-menu-right"></i><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><i class="glyphicon glyphicon-menu-right"></i><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><i class="glyphicon glyphicon-menu-right"></i><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <?php } ?>
        <div class="col-sm-2">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><i class="glyphicon glyphicon-menu-right"></i><a href="index.php?route=account/login">Login</a></li>
          <li><i class="glyphicon glyphicon-menu-right"></i><a href="index.php?route=account/register">Register</a></li>
          <li><i class="glyphicon glyphicon-menu-right"></i><a href="index.php?route=account/password">Password Reset</a></li>
        </ul>
      </div>
      
      <div class="col-sm-3">
      <div class="social-module">
      <h5>Social</h5>
      <div class="social-icon">
        <img class="hvr-bounce-in" src="catalog/view/theme/roncurrie/image/facebook.png" alt="">
        <img class="hvr-bounce-in" src="catalog/view/theme/roncurrie/image/twitter.png" alt="">
        <img class="hvr-bounce-in" src="catalog/view/theme/roncurrie/image/google.png" alt="">
        </div><!-- social-icon -->
      </div><!-- col-sm-3 -->
      </div><!-- social-module -->

      <div class="col-sm-3 footer-contact">
      <h5>Contact Us</h5>
      <?php echo html_entity_decode($address); ?><br>
      Tel: <a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a><br>
      Email: <a href="<?php echo $email; ?>"><?php echo $email; ?></a><br>
      </div><!-- col-sm-2 -->
    </div>
    
  </div>
  </div>
</footer>

<div class="mini-footer">
<div class="container">
  <h5>Copyright © <?php echo date('Y'); ?> Ron Currie and Sons Ltd.</h5>
</div><!-- container -->
</div><!-- mini-footer -->


<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>